import time
import concurrent.futures
import utils
import websites
NUM_WORKERS = 4

start_time = time.time()

with concurrent.futures.ThreadPoolExecutor(max_workers=NUM_WORKERS) as executor:
    futures = {executor.submit(utils.check_website, address) for address in websites.WEBSITE_LIST}
    concurrent.futures.wait(futures)

end_time = time.time()

print("Time for FutureSquirrel: %ssecs" % (end_time - start_time))