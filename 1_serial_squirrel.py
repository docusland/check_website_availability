import time
import utils
import websites

start_time = time.time()

for address in websites.WEBSITE_LIST:
    utils.check_website(address)

end_time = time.time()

print("Time for SerialSquirrel: %ssecs" % (end_time - start_time))