import time
import socket
import multiprocessing
import utils
import websites

NUM_WORKERS = 4

if __name__ == '__main__':
    start_time = time.time()

    with multiprocessing.Pool(processes=NUM_WORKERS) as pool:
        results = pool.map_async(utils.check_website, websites.WEBSITE_LIST)
        results.wait()

    end_time = time.time()

    print("Time for MultiProcessingSquirrel: %ssecs" % (end_time - start_time))