# Exercice : Utilisation de la bibliothèque : httlib2

Vous souhaitez automatiser une tâche permettant de voir si vos serveurs sont accessibles .
Ainsi, vous aimeriez paramétrer un script afin qu'il réalise un appel toutes les 5 minutes sur une liste d'url.

Afin de permettre à votre application de pouvoir être évolutive et maintenable facilement, vous prenez le parti de stocker au sein d'un fichier plat des URL. Exemple : websites.py

Suite à son execution, le script doit lister :

- l'ensemble des sites qui semblent être inaccessibles.
- l'ensemble des sites qui ont paramétré le cache avec les informations interessantes : etag, last-modified etc.
